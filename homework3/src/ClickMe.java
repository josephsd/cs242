import javafx.stage.Stage;
import javafx.application.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import java.io.*;
import java.net.*;
import javafx.stage.*;
import javafx.stage.FileChooser.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.scene.control.ScrollPane;


public class ClickMe extends Application{
	String user = new String("Anonymous");
	byte key = 0;
	String ipAddress = new String("localhost");
	Integer portNumber = new Integer(13699);
	public Stage mainStage;
	File sendFile;
	public static void main(String [] args){launch( args );}
	SimpleEncryptor textEncrypt = new SimpleEncryptor();
	FileEncryptor fileEncrypt = new FileEncryptor();
	
	Label label = new Label("Encrypted Text and File Messenger");
	TextFlow flow = new TextFlow();
	TextField field = new TextField();
	ScrollPane scroll = new ScrollPane(flow);
	
	BufferedReader inFromMessage;
	PrintWriter outToMessage;
	
	Socket cSocket;
	
	private boolean running = true;
	private boolean isserver = false;
	
	@Override
	public void start (Stage primaryStage){
		mainStage = primaryStage;
		settingswindow();
	}
	
	public void mainwindow(){
		
		running = true;
		if(isserver){
			serverWaitingSplash();
			initServer();
		}
		else {
			initClient();
		}
		
		
		scroll.setPrefSize(400, 250);
		flow.setMaxWidth(398);
		
		Button grabSend = new Button("Send");
		Button grabFile = new Button("Send File");
		Button settings = new Button("Settings");
		Button exit = new Button("Exit");
		
		GridPane grid = new GridPane();
		grid.add(label, 0, 0);
		grid.add(scroll, 0, 1);
		grid.add(field, 0, 2);
		grid.add(grabSend, 0, 3);
		grid.add(grabFile, 1, 3);
		grid.add(settings, 0, 4);
		grid.add(exit, 1, 4);
		
		Scene scene = new Scene(grid, 500, 400);
		
		mainStage.setTitle("Encrypted Messenger");
		mainStage.setScene(scene);
		mainStage.show();
		
		Thread tServer = new Thread(new Runnable(){
			@Override
			public void run(){
				System.out.println("Server Thread Started");
				System.out.println(running);
				while(running){
					System.out.println("Thread Working");
					
					try{ 
						String word = null;
					while((word = inFromMessage.readLine()) != null){
						//System.out.println("Thread Working");
							textEncrypt.textDecrypt(word,key);
							System.out.print(textEncrypt.encrypted);
							newMessage(textEncrypt.encrypted);
							
					}}
					catch(Exception e){}
				}
				System.out.println("Server Thread Stopped");
			}
		});
		
		
		tServer.start();
		
		grabSend.setOnAction(event -> { sendMessage(); });
		
		field.setOnAction(event -> { sendMessage(); });
		
		settings.setOnAction(event -> { settingswindow(); });
		
		exit.setOnAction(event -> { mainStage.hide(); running = false; });
		
		mainStage.setOnCloseRequest(event -> { running = false; });
		
		grabFile.setOnAction(event -> { showFileDialog(); });		
	}
	
	public void settingswindow(){
		running = false;
		Label lkey = new Label("Encryption key");
		Integer Ikey = new Integer((int)key);
	
		Label lname = new Label("Name");
		Label lipaddress = new Label("IP Address");
		Label lportnumber = new Label("Port Number");
		CheckBox cIsServer = new CheckBox("Are you the Server?");
		TextField tkey = new TextField();
		TextField tname = new TextField();
		TextField tipaddress = new TextField();
		TextField tportnumber = new TextField();
		tkey.setText(Ikey.toString());
		tname.setText(user);
		tipaddress.setText(ipAddress);
		tportnumber.setText(portNumber.toString());
		System.out.println((int)key);
		Button save = new Button("Save");
		GridPane pane = new GridPane();
		pane.add(lkey, 0, 0);
		pane.add(lname, 0, 1);
		pane.add(lipaddress, 0, 2);
		pane.add(lportnumber, 0, 3);
		pane.add(tkey, 1, 0);
		pane.add(tname, 1, 1);
		pane.add(tipaddress, 1, 2);
		pane.add(tportnumber, 1, 3);
		pane.add(cIsServer, 0, 4);
		pane.add(save, 0, 6);
		
		Scene scene = new Scene(pane, 500, 400);
		Stage stage = new Stage();
		mainStage.setScene(scene);
		mainStage.setTitle("Settings");
		mainStage.show();
		
		save.setOnAction(event -> {
			key = (byte)Integer.parseInt(tkey.getText());
			user = tname.getText();
			
			ipAddress = tipaddress.getText();
			portNumber = Integer.parseInt(tportnumber.getText());
			isserver = cIsServer.isSelected();
			mainwindow();
			
		});		
	}

	public void showFileDialog(){
		Stage stage = new Stage();
		
		FileChooser fileChooser = new FileChooser();
 		fileChooser.setTitle("Get Text File");
 		fileChooser.getExtensionFilters().addAll(
        		 new ExtensionFilter("Text Files", "*.txt"),
				 new ExtensionFilter("All Files", "*.*"));
		sendFile = fileChooser.showOpenDialog(stage);
		
		String words = new String(user + ": Sent a file saved as " + user + "EncryptedFile\n");
		Text text = new Text(words);
		flow.getChildren().addAll(text);
		scroll.setVvalue(1.0);
		
		textEncrypt.textEncrypt(words, key);
		System.out.println(textEncrypt.encrypted);
		System.out.println(sendFile);
	}
	
	public void sendMessage(){
		String words = new String(user + ": " + field.getText() + "\n");
		field.clear();
		newMessage(words);
		
		textEncrypt.textEncrypt(words,key);
		outToMessage.println(textEncrypt.encrypted);
	}

	public void initServer(){
		try {
			
			ServerSocket ss = new ServerSocket(portNumber);
			System.out.println("serverWaitinfgSpapsnatheous");
			Socket serverSocket = ss.accept( ); // waits for client
		
			inFromMessage = new BufferedReader ( new InputStreamReader( serverSocket.getInputStream()));
		
			outToMessage = new PrintWriter ( serverSocket.getOutputStream(), true /* autoFlush */);
			
			}
			catch(Exception e){}
	}
	public void initClient(){   
		try {
			Socket clientSocket = new Socket(ipAddress, portNumber); 
			inFromMessage = new BufferedReader ( new InputStreamReader( clientSocket.getInputStream()));
			outToMessage = new PrintWriter(clientSocket.getOutputStream(),true);
			}
			catch(Exception e){}	
		}
	public void chatThread(){
		Thread cServer = new Thread(new Runnable(){
			@Override
			public void run(){
				System.out.println("Server Thread Started");
				try{
				String word = null;
				while((word = inFromMessage.readLine()) != null){
					//System.out.println("Thread Working");
						textEncrypt.textDecrypt(word,key);
						System.out.print(textEncrypt.encrypted);
				}}
				catch(Exception e){}
				}
		});
		
		cServer.start();
	}
	public void serverWaitingSplash(){
		String message = new String("Waiting for Connection");
		System.out.println(message);
		Label wait = new Label(message);
		GridPane pane = new GridPane();
		pane.add(wait, 0, 0);
		Scene scene = new Scene(pane,500,400);
		
		mainStage.setTitle(message);
		mainStage.setScene(scene);
		mainStage.show();
				
	}
	public void newMessage(String message){
		Text text = new Text(message);
		flow.getChildren().addAll(text);
		scroll.setVvalue(1.0);
	}
}
